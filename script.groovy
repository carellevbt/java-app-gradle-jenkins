#!/usr/bin/env groovy

def permission() {
    echo "permission granted"
    sh 'chmod +x gradlew'
}

def buildjar() {
    echo "building jar file"
    sh './gradlew build'
}

def image() {
    echo "building image"
    sh 'docker build -t apps:gradle3 .'
}

def pushAWS() {
    echo "pushing image to AWS"
    withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: 'AWS', accessKeyVariable: 'AWS_ACCESS_KEY_ID', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]){
        sh "aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 103577711798.dkr.ecr.us-east-1.amazonaws.com"
        sh 'docker tag apps:gradle3 103577711798.dkr.ecr.us-east-1.amazonaws.com/apps:gradle3'
        sh 'docker push 103577711798.dkr.ecr.us-east-1.amazonaws.com/apps:gradle3'
    }
}

return this